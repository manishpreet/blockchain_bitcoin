package com.example.balwinderbadgal.blockchain_bitcoin;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
/**
 * Created by balwinder on 1/12/17.
 */

public class Pager extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    int tabCount;

    //Constructor to the class
    public Pager(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                Home home = new Home();
                return home;
            case 1:
                SendBitcoin sendBitcoin = new SendBitcoin();
                return sendBitcoin;
                case 2:
                    RecieveBitcoin recieveBitcoin = new RecieveBitcoin();
                return recieveBitcoin;
                case 3:
                Transactions transactions = new Transactions();
                return transactions;
            default:
                return null;
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return tabCount;
    }
}